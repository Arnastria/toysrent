"""toysrent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from fitur_1_4.views import *

urlpatterns = [
    path('', include('fitur_5_8.urls'), name="fitur_5_8"),
    path('', include('fitur_13_16.urls'), name="fitur_13_16"),
    path('', include('fitur_9_12.urls'), name="fitur_9_12"),
    path('', include('mainApp.urls'), name="main_page"),
    path('', include('fitur_1_4.urls'), name="fitur_1_4"),
    path('admin/', admin.site.urls),
]