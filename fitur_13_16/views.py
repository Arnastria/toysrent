from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .models import *

def get_as_dict(cursor):
    #print(cursor)
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def daftar_pengiriman_view(request):
    isAdmin = request.session['response']['login_as_admin']
    print("ADMIN",isAdmin)
    if(not isAdmin):
        no_ktp = request.session['response']['no_ktp_yang_lagi_login']
        a = get_as_dict(pengiriman.get_daftar_pengiriman(no_ktp))
    else :
        a = get_as_dict(pengiriman.get_daftar_pengiriman_ADMIN())
    
    nama_barang = []
    for row in a :
        nama_barang.append((get_as_dict(pengiriman.get_nama_barang_dikirim(row.get('no_resi'))))[0])

    print("=========")
    print(nama_barang)
    print("==============")
    print(a)

    for row2 in a:
        nama_item = []
        for row in nama_barang:
            if (row2.get('no_resi') == row.get('no_resi')):
                nama_item.append(row.get('nama_item'))                
            row2.update({'barang_kiriman':nama_item})

    context = {
        "daftar_pengiriman" : a   
    }
    print("=====RESULT====")
    print(a)
    return render(request,'daftarPengiriman.html',context)

def buat_pengiriman_viewID(request,id):
    isAdmin = request.session['response']['login_as_admin']
    print("ADMIN",isAdmin)
    alamat = []
    
    if(isAdmin):
        alamat = get_as_dict(pengiriman.get_alamat_ADMIN())
    else :
        no_ktp = request.session['response']['no_ktp_yang_lagi_login']
        alamat = get_as_dict(pengiriman.get_alamat(no_ktp))
    
    
    context = {
        "id_pemesanan" :id,
        "barang_pesanan" : get_as_dict(pengiriman.get_barang_pesanan(id)),
        "alamat" : alamat
    }
    print(context)
    return render(request,'buatPengiriman.html',context)
    
def buat_pengiriman_pilih_pemesanan_view(request):
    isAdmin = request.session['response']['login_as_admin']
    print("ADMIN",isAdmin)
    id_pesanan = []
    if(isAdmin) :
        id_pesanan = get_as_dict(pengiriman.get_id_pemesanan_ADMIN())
    else :
        no_ktp = request.session['response']['no_ktp_yang_lagi_login']
        id_pesanan = get_as_dict(pengiriman.get_id_pemesanan(no_ktp))
    
    context = {
        "id_pesanan" : id_pesanan
    }

    print(context)
    return render(request,'pilih_pemesanan.html',context)

def update_pengiriman(request,no_resi):
    no_resi_res = get_as_dict(pengiriman.get_data_pengiriman(no_resi))
    id_pemesanan = (no_resi_res[0].get('id_pemesanan'))
    tanggal = str((no_resi_res[0].get('tanggal')))

    isAdmin = request.session['response']['login_as_admin']
    print("ADMIN",isAdmin)
    alamat = []
    
    if(isAdmin):
        alamat = get_as_dict(pengiriman.get_alamat_ADMIN())
    else :
        no_ktp = request.session['response']['no_ktp_yang_lagi_login']
        alamat = get_as_dict(pengiriman.get_alamat(no_ktp))
    
    context = {
        "no_resi" : no_resi,
        "barang_pesanan" : get_as_dict(pengiriman.get_barang_pesanan_dikirim(id_pemesanan)),
        "data_pengiriman" : no_resi_res[0],
        "tanggal" : tanggal,
        "alamat" : alamat
    }
    return render(request,'updatePengiriman.html',context)

def daftar_review_view(request):
    no_resi_arr = []
    review_arr = []
    no_resi_doi = []
    
    isAdmin = request.session['response']['login_as_admin']
    print("ADMIN",isAdmin)
    if(not isAdmin):
        no_ktp = request.session['response']['no_ktp_yang_lagi_login']
        no_resi_doi = get_as_dict(review.get_no_resi(no_ktp))
    else :
        no_resi_doi = get_as_dict(review.get_no_resi_ADMIN())
    
    for row in no_resi_doi :
        review_arr.append(get_as_dict(review.get_review(row.get('no_resi')))[0])

    context = {
        "review_data" : review_arr
    }

    return render(request,'daftarReview.html',context)

def buat_review_view(request,no_resi):
    context = {
        "no_resi" : no_resi,
        "barang_dikirim" : get_as_dict(review.get_barang_dikirim(no_resi)),
    }
    print(context)
    return render(request,'buatReview.html',context)

def update_review_view(request,no_resi):

    context = {
        "no_resi" : no_resi,
        "data_review" : get_as_dict(review.get_review(no_resi))[0],
    }
    print(context)
    return render(request,'updateReview.html',context)

def hapus_pengiriman(request,no_resi):
    pengiriman.delete_pengiriman(no_resi)
    return redirect("daftar_pengiriman")

def hapus_review(request,no_resi):
    review.delete_review(no_resi)
    return redirect("daftar_review")

def create_pengiriman(request,id_pemesanan,id_barang,alamat,tanggal,metode):
    no_ktp = request.session['response']['no_ktp_yang_lagi_login']
    pengiriman.create_pengiriman(id_pemesanan,id_barang,alamat,tanggal,metode,no_ktp)
    return redirect("daftar_pengiriman")

def create_review(request,no_resi,id_barang,review):
    no_ktp = request.session['response']['no_ktp_yang_lagi_login']
    review.create_review(no_resi,id_barang,review)
    return redirect("daftar_review")

def updates_pengiriman(request,id_pemesanan,id_barang,alamat,tanggal,metode):
    no_ktp = request.session['response']['no_ktp_yang_lagi_login']
    pengiriman.update_pengiriman(id_pemesanan,id_barang,alamat,tanggal,metode,no_ktp)
    return redirect("daftar_pengiriman")

def updates_review(request,no_resi,id_barang,review):
    no_ktp = request.session['response']['no_ktp_yang_lagi_login']
    review.update_review(no_resi,id_barang,review)
    return redirect("daftar_review")