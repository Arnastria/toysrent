from django.db import models
from django.db import connection
import random

# Create your models here.
SCHEMA = "toys_rent."

PENGIRIMAN = SCHEMA+"pengiriman"
PEMESANAN = SCHEMA+"pemesanan"
BARANG = SCHEMA+"barang"
BARANG_DIKIRIM = SCHEMA+"barang_dikirim"
BARANG_PESANAN = SCHEMA+"barang_pesanan"
BARANG_DIKEMBALIKAN = SCHEMA+"barang_dikembalikan"
PENGGUNA = SCHEMA+"pengguna"
ALAMAT = SCHEMA+"alamat"

cursor = connection.cursor()

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

class pengiriman :    
    def get_id_pemesanan(id_anggota):
        query = "SELECT id_pemesanan,nama_lengkap,status from %s,%s where no_ktp_pemesan = '%s' and no_ktp = no_ktp_pemesan;" %(PEMESANAN,PENGGUNA,id_anggota)
        cursor.execute(query)
        return cursor
    
    def get_id_pemesanan_ADMIN():
        query = "SELECT id_pemesanan,nama_lengkap,status from %s,%s where no_ktp = no_ktp_pemesan;" %(PEMESANAN,PENGGUNA)
        cursor.execute(query)
        return cursor

    def get_barang_pesanan(id_pemesanan):
        print("get_barang_pesanan")
        query = "SELECT DISTINCT a.id_barang,b.nama_item from %s a,%s b where id_pemesanan ='%s' and a.id_barang =b.id_barang;" %(BARANG_PESANAN,BARANG,id_pemesanan)
        cursor.execute(query)
        return cursor

    def get_barang_pesanan_dikirim(id_pemesanan):
        print("get_barang_pesanan_dikirim")
        query = "SELECT a.id_barang,b.nama_item from %s a,%s b where id_pemesanan ='%s' and a.id_barang =b.id_barang and a.status = 'DIKIRIM';" %(BARANG_PESANAN,BARANG,id_pemesanan)
        cursor.execute(query)
        return cursor

    def get_alamat(id_anggota):
        print("get_alamat")
        query = "SELECT concat(nama,' jalan ',jalan,' no ',nomor,', ',kota,', ',kodepos) alamat_lengkap from %s where no_ktp_anggota = '%s';" %(ALAMAT,id_anggota)
        cursor.execute(query)
        return cursor
    
    def get_alamat_ADMIN():
        print("get_alamat")
        query = "SELECT concat(nama,' jalan ',jalan,' no ',nomor,', ',kota,', ',kodepos) alamat_lengkap from %s;" %(ALAMAT)
        cursor.execute(query)
        return cursor
        
    def get_daftar_pengiriman(id_anggota):
        print("get_daftar_pengiriman")
        query = "SELECT no_resi, id_pemesanan, ongkos,tanggal from %s where no_ktp_anggota = '%s';"%(PENGIRIMAN,id_anggota)
        cursor.execute(query)
        return cursor

    def get_daftar_pengiriman_ADMIN():
        print("get_daftar_pengiriman ADMIN")
        query = "SELECT no_resi, id_pemesanan, ongkos,tanggal from %s;"%(PENGIRIMAN)
        cursor.execute(query)
        return cursor
        
    def get_nama_barang_dikirim(no_resi):
        query = "SELECT bk.no_resi, nama_item from %s b, %s bk where b.id_barang = bk.id_barang and bk.no_resi = '%s';" %(BARANG,BARANG_DIKIRIM,no_resi)
        cursor.execute(query)
        return cursor

    def get_data_pengiriman(no_resi):
        query = "SELECT * from %s where no_resi = '%s';" %(PENGIRIMAN,no_resi)
        cursor.execute(query)
        return cursor
    
    def delete_pengiriman(no_resi):
        query = "DELETE from %s where no_resi = '%s';" %(PENGIRIMAN,no_resi)
        cursor.execute(query)
        return cursor
    
    def create_pengiriman(id_pemesanan,id_barang,alamat,tanggal,metode,no_ktp_anggota):
        resi = random.randint(100000,999999)
        query = "INSERT INTO '%s' VALUES('%s','%s','%s','%f','%s','%s','%s');" %(PENGIRIMAN,resi,id_pemesanan,metode,50000,tanggal,no_ktp_anggota,alamat)
        cursor.execute(query)
        return cursor

    def update_pengiriman(id_pemesanan,id_barang,alamat,tanggal,metode,no_ktp_anggota):
        query = "UPDATE '%s' set alamat = '%s', tanggal = '%s', metode = '%s' where no_resi = '%s';" %(PENGIRIMAN,alamat,tanggal,metode,id_pemesanan)
        cursor.execute(query)
        return cursor

class review:
    def get_review(no_resi):
        query = "SELECT bk.no_resi,nama_item nama_barang, bk.id_barang,tanggal_REVIEW, review from %s BK, %s B where b.id_barang = bk.id_barang and bk.no_resi = '%s'" %(BARANG_DIKIRIM,BARANG,no_resi)
        cursor.execute(query)
        return cursor

    def get_no_resi(id_anggota):
        query = "SELECT no_resi from %s where no_ktp_anggota = '%s';" %(PENGIRIMAN,id_anggota) 
        cursor.execute(query)
        return cursor

    def get_no_resi_ADMIN():
        query = "SELECT no_resi from %s;" %(PENGIRIMAN) 
        cursor.execute(query)
        return cursor

    def get_barang_dikirim(no_resi):
        query = "SELECT DISTINCT nama_item, bk.id_barang from %s bk, %s b where bk.id_barang = b.id_barang and bk.no_resi = '%s' " %(BARANG_DIKIRIM,BARANG,no_resi)
        cursor.execute(query)
        return cursor
    
    def delete_review(no_resi):
        query = "DELETE FROM %s where no_resi = '%s';" %(BARANG_DIKIRIM,no_resi)
        cursor.execute(query)
        return cursor 

    def create_review(no_resi,id_barang,review):
        query = "INSERT INTO '%s' VALUES('%s','%s','%s','%s') where id;" %(BARANG_DIKIRIM,no_resi,id_barang,tanggal,review)
        cursor.execute(query)
        return cursor

    def update_review(no_resi,id_barang,review):
        query = "UPDATE '%s' set review = '%s' where no_resi = '%s' and id_barang = '%s';"%(BARANG_DIKIRIM,review,no_resi,id_barang)
        cursor.execute(query)
        return cursor