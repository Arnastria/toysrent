from django.urls import re_path,path
from .views import *


#url for app
urlpatterns = [
    path('daftar-pengiriman/', daftar_pengiriman_view, name = 'daftar_pengiriman'),
    path('buat-pengiriman/', buat_pengiriman_pilih_pemesanan_view, name = 'buat_pengiriman'),
    path('buat-pengiriman/<int:id>/', buat_pengiriman_viewID, name = 'buat_pengiriman_id'),
    path('daftar-review/', daftar_review_view, name = 'daftar_review'),
    path('buat-review/<int:no_resi>/', buat_review_view, name = 'buat_review'),
    path('update-pengiriman/<int:no_resi>', update_pengiriman, name = 'update_pengiriman'),
    path('update-review/<int:no_resi>/', update_review_view, name = 'update_review'),
    path('hapus-pengiriman/<int:no_resi>/',hapus_pengiriman,name ='hapus_pengiriman'),
    path('create-pengiriman/<str:id_pemesanan>/<str:id_barang>/<str:alamat>/<str:tanggal>/<str:metode>/',create_pengiriman,name ='create-pengiriman'),
    path('create-review/<str:no_resi>/<str:id_barang>/<str:review>/',create_review,name='create-review'),
    path('updates-pengiriman/<str:id_pemesanan>/<str:id_barang>/<str:alamat>/<str:tanggal>/<str:metode>/',updates_pengiriman,name ='create-pengiriman'),
    path('updates-review/<str:no_resi>/<str:id_barang>/<str:review>/',create_review,name='create-review'),
]
