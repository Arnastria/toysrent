from django.urls import re_path,path
from .views import *


#url for app yoi bos
urlpatterns = [
    path('tambah-barang/', tambah_update_barang, name = 'tambah_barang'),
    re_path(r'^update-barang/(?P<id_barang>\w+)/$', tambah_update_barang, name = 'update_barang'),
    path('chat/', chat, name = 'chat'),
    path('list-barang/', list_barang, name = 'list_barang'),
    re_path(r'^detail-barang/(?P<id_barang>\w+)/$', detail_barang, name = 'detail_barang'),
    re_path(r'^hapus-barang/(?P<id_barang>\w+)/$', hapus_barang, name = 'hapus_barang'),
    path('chat/pengguna/', chatpengguna, name = 'chatpengguna'),
    path('chat/admin/', chatadmin, name = 'chatadmin'),
    re_path(r'^chat/admin/(?P<no_ktp_pengguna>\d+)/$', detailchatadmin, name = 'detailchatadmin'),
]
