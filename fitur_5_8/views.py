from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from django.core.serializers.json import DjangoJSONEncoder
import datetime
import json
import random

def tambah_update_barang(request, id_barang=None):
    response = {}
    if request.method == "GET":
        cursor = connection.cursor()
        if id_barang:
            cursor.execute("SELECT * FROM toys_rent.barang WHERE id_barang='%s'" % (id_barang))
            response.update(dictfetchall(cursor)[0])
            for level in ["Bronze", "Silver", "Gold"]:
                cursor.execute("SELECT * FROM toys_rent.info_barang_level WHERE id_barang='%s' AND nama_level='%s'" % (id_barang, level))
                response.update({level: dictfetchall(cursor)[0]})
        cursor.execute("SELECT p.no_ktp, p.nama_lengkap FROM toys_rent.pengguna AS p, toys_rent.anggota AS a WHERE p.no_ktp = a.no_ktp")
        daftar_pengguna = dictfetchall(cursor)
        response['daftar_pengguna'] = daftar_pengguna
        cursor.execute("SELECT * from toys_rent.item")
        daftar_item = dictfetchall(cursor)
        response['daftar_item'] = daftar_item
        return render(request,'tambahBarang.html', response)
    if request.method == "POST":
        id_barang_ori = id_barang # untuk update
        cursor = connection.cursor()
        login_data = request.POST.dict()
        if not id_barang_ori:
            id_barang = request.POST["id_barang"]
        nama_item = request.POST["nama_item"]
        warna = request.POST["warna"]
        url_foto = request.POST["url_foto"]
        kondisi = request.POST["kondisi"]
        lama_penggunaan = request.POST["lama"]
        nama_penyewa = request.POST["nama_penyewa"]

        if not id_barang_ori:
            cursor.execute(
                "INSERT INTO toys_rent.barang values ('%s','%s','%s','%s','%s','%s','%s')"
                % (id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, nama_penyewa)
            )
        else:
            cursor.execute(
                "UPDATE toys_rent.barang SET nama_item='%s', warna='%s', url_foto='%s', kondisi='%s', lama_penggunaan='%s', no_ktp_penyewa='%s' WHERE id_barang='%s'"
                % (nama_item, warna, url_foto, kondisi, lama_penggunaan, nama_penyewa, id_barang)
            )
        
        bronze_royalti = request.POST["bronze_royalti"]
        bronze_harga = request.POST["bronze_harga"]
        silver_royalti = request.POST["silver_royalti"]
        silver_harga = request.POST["silver_harga"]
        gold_royalti = request.POST["gold_royalti"]
        gold_harga = request.POST["gold_harga"]

        if not id_barang_ori:
            cursor.execute(
                "INSERT INTO toys_rent.info_barang_level values ('%s','Bronze',%s,%s),('%s','Silver',%s,%s),('%s','Gold',%s,%s)"
                % (
                    id_barang, bronze_harga, bronze_royalti,
                    id_barang, silver_harga, silver_royalti,
                    id_barang,gold_harga,gold_royalti
                )
            )
        else:
            cursor.execute(
                "UPDATE toys_rent.info_barang_level SET harga_sewa='%s', porsi_royalti='%s' WHERE id_barang='%s' AND nama_level='Bronze'"
                % (bronze_harga, bronze_royalti, id_barang)
            )
            cursor.execute(
                "UPDATE toys_rent.info_barang_level SET harga_sewa='%s', porsi_royalti='%s' WHERE id_barang='%s' AND nama_level='Silver'"
                % (silver_harga, silver_royalti, id_barang)
            )
            cursor.execute(
                "UPDATE toys_rent.info_barang_level SET harga_sewa='%s', porsi_royalti='%s' WHERE id_barang='%s' AND nama_level='Gold'"
                % (gold_harga, gold_royalti, id_barang)
            )
        return HttpResponseRedirect('/list-barang/')

def list_barang(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT nama from toys_rent.kategori")
    kategori = dictfetchall(cursor)
    response['kategori'] = kategori

    query = "SELECT b.*, ki.nama_kategori FROM (SELECT * FROM toys_rent.kategori_item) ki JOIN toys_rent.barang b ON b.nama_item = ki.nama_item"
    if request.GET.get('filter'):
        query += " AND ki.nama_kategori = '%s'" % (request.GET['filter'])
    if request.GET.get('sort'):
        if request.GET['sort'] == 'kategori':
            query += " ORDER BY ki.nama_kategori"
        if request.GET['sort'] == 'nama':
            query += " ORDER BY b.nama_item"
    
    cursor.execute(query)
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request,'listbarang.html',response)

def detail_barang(request, id_barang):
    cursor = connection.cursor()
    cursor.execute("SELECT * from toys_rent.barang where id_barang='%s'" % (id_barang))
    response = dictfetchall(cursor)[0]
    return render(request,'detailbarang.html', response)

def hapus_barang(request, id_barang):
    cursor = connection.cursor()
    cursor.execute("DELETE from toys_rent.barang where id_barang='%s'" % (id_barang))
    return HttpResponseRedirect('/list-barang/')


def chatpengguna(request):
    response = {}
    if request.method=="POST":
        cursor = connection.cursor()
        login_data = request.POST.dict()
        pesan = request.POST["pesan"]

        cursor.execute(
            "INSERT INTO toys_rent.chat values ('%s','%s','%s','%s','%s')"
            % (random.randint(1,100000),pesan,datetime.datetime.now(),request.session['response']['no_ktp_yang_lagi_login'],'2134567891')
        )
        return HttpResponseRedirect('/chat/pengguna/')
  
    else:
        cursor = connection.cursor()
        cursor.execute("SELECT * from toys_rent.chat where no_ktp_anggota='%d' order by date_time asc" % (request.session['response']['no_ktp_yang_lagi_login']))
        response["chat"] = dictfetchall(cursor)
    return render(request,'chatpengguna.html',response)

def detailchatadmin(request, no_ktp_pengguna):
    response = {}
    if request.method=="POST":
        cursor = connection.cursor()
        login_data = request.POST.dict()
        pesan = request.POST["pesan"]

        cursor.execute(
            "INSERT INTO toys_rent.chat values ('%s','%s','%s','%s','%s')"
            % (random.randint(1,100000),pesan,datetime.datetime.now(),no_ktp_pengguna,request.session['response']['no_ktp_yang_lagi_login'])
        )
        return HttpResponseRedirect('/chat/admin/%s/' % (no_ktp_pengguna, ))
    else:
        cursor = connection.cursor()
        cursor.execute("SELECT * from toys_rent.chat where no_ktp_anggota='%s' order by date_time asc" % (no_ktp_pengguna))
        response["chat"] = dictfetchall(cursor)
        return render(request,'chatpengguna.html',response)

def chat(request):
    isAdmin = request.session['response']['login_as_admin']
    if(not isAdmin):
        return HttpResponseRedirect('/chat/pengguna/')
    else :
        return HttpResponseRedirect('/chat/admin/')


def chatadmin(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT distinct no_ktp_anggota from toys_rent.chat")
    response["chat"] = dictfetchall(cursor)
    return render(request,'chatadmin.html',response)


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
