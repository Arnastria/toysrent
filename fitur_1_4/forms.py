from django import forms

ktp_field = forms.IntegerField(max_value=int('9' * 20),widget = forms.NumberInput(attrs = {'placeholder':'Nomor KTP'}))
email_field = forms.EmailField(max_length=50, widget = forms.EmailInput(attrs = {'placeholder': 'Email'}))
nama_field = forms.CharField(strip=True, max_length=50)
tgl_lahir_field = forms.DateField()
no_telp_field = forms.CharField(required=False, max_length=20)
alamat_field = forms.CharField(required=False, widget=forms.Textarea)

class LoginForm(forms.Form):
    ktp = ktp_field
    email = email_field