from django.shortcuts import render, redirect, reverse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from django.core.serializers.json import DjangoJSONEncoder
import json
from fitur_1_4 import forms
response = {}

# Create your views here.
def login_page(request):
    response["all_anggota"] = get_as_dict(get_all("anggota"))
    response["all_admin"] = get_as_dict(get_all("admin"))
    response["form"] = forms.LoginForm()
    if (request.method == 'POST'):
        response["form"] = forms.LoginForm(request.POST)
        if response["form"].is_valid():
            ktp = response["form"].cleaned_data['ktp']
            email = response["form"].cleaned_data['email']
            if authenticate(request,ktp=ktp,email=email):
                response["form"] = None
                request.session['response'] = response
                request.session['ada_yang_lagi_login'] = True
                return redirect("redirect")
        return render(request,'login.html',response)
    return render(request,'login.html',response)

def logout(request):
    request.session.flush()
    return redirect("login_page")

def admin_page(request):
    return render(request,'admin.html')

def anggota_page(request):
    return render(request,'anggota.html')

def create_item_page(request):
    return render(request,'create_item.html')

def update_item_page(request):
    return render(request,'update_item.html')

# DAFTAR ITEM (ANGGOTA)
def daftar_item_page(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from toys_rent.kategori_item k, toys_rent.barang b WHERE k.nama_item=b.nama_item")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request,'daftar_item.html', response)

def lihat_brg(request, namaItem):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT url_foto FROM toys_rent.barang WHERE nama_item = '"+namaItem+"'")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'lihat_barang.html', response)

# DAFTAR ITEM (ADMIN)
def daftar_item_admin_page(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * from toys_rent.kategori_item")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request,'daftar_item_admin.html', response)

def add_item_multiselect(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT nama FROM toys_rent.kategori")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request, 'create_item.html', response)

@csrf_exempt
def add_item(request):
    if(request.method == 'POST'):
        cursor = connection.cursor()
        nama = request.POST["nama"]
        deskripsi = request.POST["deskripsi"]
        usia_dari = request.POST["usia_dari"]
        usia_sampai = request.POST["usia_sampai"]
        bahan = request.POST["bahan"]
        kategori = request.POST.getlist("kategori")
        cursor.execute("INSERT INTO TOYS_RENT.ITEM (nama,deskripsi,usia_dari,usia_sampai,bahan) VALUES('"+nama+"', '"+deskripsi+"', '"+str(usia_dari)+"', '"+str(usia_sampai)+"', '"+bahan+"')")
        for i in kategori:
            cursor.execute("INSERT INTO TOYS_RENT.KATEGORI_ITEM (nama_item, nama_kategori) VALUES('"+nama+"', '"+i+"')")
    return redirect(reverse('daftar_item_admin_page'))

def updt_item(request, namaItem):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM toys_rent.item WHERE nama = '"+namaItem+"'")
    update = dictfetchall(cursor)

    cursor.execute("SELECT nama from toys_rent.kategori")
    kategori = dictfetchall(cursor)

    cursor.execute("SELECT * FROM toys_rent.kategori_item where nama_item = '"+namaItem+"'")
    selected_kategori = dictfetchall(cursor)

    response['update'] = update[0]
    response['kategori'] = kategori
    response['selected_kategori'] = [x['nama_kategori'] for x in selected_kategori]
    return render(request, 'update_item.html', response)

@csrf_exempt
def update_item(request, namaItem):
    if (request.method == 'POST'):
        cursor = connection.cursor()
        nama = request.POST["nama"]
        deskripsi = request.POST["deskripsi"]
        usia_dari = request.POST["usia_dari"]
        usia_sampai = request.POST["usia_sampai"]
        bahan = request.POST["bahan"]
        kategori = request.POST.getlist("kategori")
        cursor.execute("UPDATE toys_rent.item SET nama = '"+nama+"', deskripsi = '"+deskripsi+"', usia_dari = '"+usia_dari+"', usia_sampai = '"+usia_sampai+"', bahan = '"+bahan+"' WHERE nama = '"+namaItem+"';")
        cursor.execute("DELETE FROM toys_rent.kategori_item WHERE nama_item = '"+namaItem+"';")
        for i in kategori:
            cursor.execute("INSERT INTO toys_rent.kategori_item (nama_item,nama_kategori) VALUES ('"+str(namaItem)+"','"+str(i)+"')")
    return redirect(reverse('daftar_item_admin_page'))

def delete_item(request, namaItem):
    response = {}
    cursor = connection.cursor()
    cursor.execute("DELETE FROM toys_rent.item WHERE nama = '"+namaItem+"';")
    cursor.execute("DELETE FROM toys_rent.kategori_item WHERE nama_item = '"+namaItem+"';")
    return redirect(reverse('daftar_item_admin_page'))

def profil_page(request):
    response = {}
    isAdmin = request.session['response']['login_as_admin']
    ktp_user = request.session['response']['no_ktp_yang_lagi_login']

    if(isAdmin):
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM toys_rent.pengguna p WHERE p.no_ktp = '"+str(ktp_user)+"';")
        daftar = dictfetchall(cursor)
        response['daftar'] = daftar
        return render(request,'profil_admin.html', response)
    else:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM toys_rent.pengguna p, toys_rent.anggota a, toys_rent.alamat l WHERE p.no_ktp=a.no_ktp AND a.no_ktp=l.no_ktp_anggota AND p.no_ktp = '"+str(ktp_user)+"' AND l.no_ktp_anggota = '"+str(ktp_user)+"';")
        daftar = dictfetchall(cursor)
        response['daftar'] = daftar
        return render(request,'profil_anggota.html', response)

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def get_all(table_name):
    cursor = connection.cursor()
    schema = "toys_rent."
    all_users = schema+table_name
    query = "select * from %s" %(all_users)
    cursor.execute(query)
    return cursor
def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns,row)))
    return results

def authenticate(request, **kwargs):
    response["all_anggota"] = get_as_dict(get_all("anggota"))
    response["all_admin"] = get_as_dict(get_all("admin"))
    login_as_admin = False
    login_as_anggota = False
    admin_who_logins = None
    anggota_who_logins = None
    for anggota in response["all_anggota"]:
        if anggota['no_ktp'] == str(kwargs['ktp']):
            login_as_anggota = True
            anggota_who_logins = anggota

    for admin in response["all_admin"]:
        if admin['no_ktp'] == str(kwargs['ktp']):
            login_as_admin = True
            admin_who_logins = admin
    if admin_who_logins is not None :
        request.session['admin_auth'] = json.dumps(admin_who_logins, cls = DjangoJSONEncoder)
        request.session.save()
    if anggota_who_logins is not None:
        request.session['anggota_auth'] = json.dumps(anggota_who_logins, cls = DjangoJSONEncoder)
        request.session.save()
    response["login_as_admin"] = login_as_admin
    response['login_as_anggota'] = login_as_anggota
    if login_as_admin or login_as_anggota:
        response['no_ktp_yang_lagi_login'] = kwargs['ktp']
        return True
    response['no_ktp_yang_lagi_login'] = None
    return False



    
    
    

    
    


    
