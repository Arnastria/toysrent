from django.urls import re_path,path
from .views import *


#url for app
urlpatterns = [
    path('login/', login_page, name = 'login_page'),
    path('logout/',logout, name = 'logout'),
    path('admin_forms/', admin_page, name = 'admin_page'),
    path('anggota_forms/', anggota_page, name = 'anggota_page'),
    path('create_item/', create_item_page, name = 'create_item_page'),
    path('update_item/', update_item_page, name = 'update_item_page'),
    path('daftar_item_agt/', daftar_item_page, name = 'daftar_item_page'),
    path('profil/', profil_page, name = 'profil_page'),
    path('daftar_item_admin/', daftar_item_admin_page, name = 'daftar_item_admin_page'),

    path('add_item_multiselect/', add_item_multiselect, name = 'add_item_multiselect'),
    path('add_item_multiselect/add_item', add_item, name = 'add_item'),
    path('updt_item/(?P<namaItem>\d+)/', updt_item, name = 'updt_item'),
    path('update_item/(?P<namaItem>\d+)/', update_item, name = 'update_item'),
    path('delete_item/(?P<namaItem>\d+)/', delete_item, name = 'delete_item'),

    path('lihat_barang/(?P<namaItem>\d+)/', lihat_brg, name = 'lihat_brg'),
]