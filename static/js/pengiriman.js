
$(document).ready(function () {
    var pageURL = $(location).attr("href");
    $("#button-submit").click(function () {
        var id_barang = $( "#input-barang" ).val();
        var alamat = $( "#input-alamat" ).val();
        var tanggal = $( "#input-tanggal" ).val();
        var metode = $( "#input-metode" ).val();
        var id_pemesanan = $( "#id-pemesanan" ).html().trim();
        if(id_barang && alamat && tanggal && metode){
            $('#confirm-modal').html("Biaya total adalah Rp.50.000,- NET selama masa promo");
            $('#exampleModal').modal('show');
            if(pageURL.includes("update")){
                document.getElementById("btn-create-pengiriman").href="/create-pengiriman/"+id_pemesanan+"/"+id_barang.split("-")[1]+"/"+alamat.split(" ")[0]+"/"+tanggal+"/"+metode+"/"; 
            }else{
                document.getElementById("btn-create-pengiriman").href="/updates-pengiriman/"+id_pemesanan+"/"+id_barang.split("-")[1]+"/"+alamat.split(" ")[0]+"/"+tanggal+"/"+metode+"/"; 
            }
        }else{
            $('#confirm-modal').html("Tolong isi semua dengan benar");
            document.getElementById("btn-create-pengiriman").href="#";
            $('#exampleModal').modal('show');
        }
        console.log(id_barang.split("-")[1]+alamat+tanggal+metode);
    });    

    $("#button-submit2").click(function () {
        var id_barang = $( "#input-id_barang" ).val();
        var review = $("#input-review" ).val();
        var no_resi = $("#no-resi").html().trim();
        if(id_barang && review){
            $('#confirm-modal').html("Isi review anda adalah : "+review);
            $('#exampleModal2').modal('show');
            if(pageURL.includes("update")){
                document.getElementById("btn-create-pengiriman").href="updates-review/"+no_resi+"/"+id_barang+"/"+review+"/";
            }else{
                document.getElementById("btn-create-pengiriman").href="create-review/"+no_resi+"/"+id_barang+"/"+review+"/";
            }
        }else{
            $('#confirm-modal').html("Tolong isi semua dengan benar");
            document.getElementById("btn-create-pengiriman").href="#";
            $('#exampleModal2').modal('show');
        }
    });  
});