from django.urls import re_path,path
from .views import *

#url for app
urlpatterns = [
    path('daftar-pemesanan-admin/', daftar_pemesanan, name = 'daftar_pemesanan'),
    path('daftar-pemesanan-anggota/', daftar_pemesanan_anggota, name = 'daftar_pemesanan_anggota'),
    path('pemesanan/', pemesanan, name = 'pemesanan'),
    path('update-pemesanan/', update_pesanan, name = 'update_pesanan'),
    path('daftar-level/', daftar_level, name = 'daftar_level'),
    path('tambah-level/', tambah_level, name = 'tambah_level'),
    path('update-level/', update_level, name = 'update-level'),
   

]