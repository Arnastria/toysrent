from django.shortcuts import render, redirect
from fitur_9_12 import forms
import datetime
from django.db import connection
from fitur_9_12 import forms
from fitur_1_4.views import get_all, get_as_dict
# Create your views here.

response = {}
id_pemesanan = 9893874897

def daftar_pemesanan(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT distinct p.id_pemesanan, p.harga_sewa, p.status, b.nama_item FROM TOYS_RENT.PEMESANAN as p, TOYS_RENT.BARANG_PESANAN as bp, TOYS_RENT.BARANG as b WHERE p.id_pemesanan=bp.id_pemesanan AND bp.id_barang=b.id_barang;")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request,'daftar_pemesanan.html', response)

#DAFTAR PEMESANAN(ANGGOTA)
def daftar_pemesanan_anggota(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT distinct p.id_pemesanan, p.harga_sewa, p.status, b.nama_item FROM TOYS_RENT.PEMESANAN as p, TOYS_RENT.BARANG_PESANAN as bp, TOYS_RENT.BARANG as b, TOYS_RENT.ANGGOTA as a WHERE p.id_pemesanan=bp.id_pemesanan AND bp.id_barang=b.id_barang AND a.no_ktp=p.no_ktp_pemesan;")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request,'daftar_pemesanan_anggota.html', response)

def pemesanan(request): 
    if len(request.session.keys())>0:
        if request.session['response']['login_as_admin']:
            response["form"] = forms.PemesananFormForAdmin()
        else:
            response["form"] = forms.PemesananFormForAnggota()
        if(request.method == 'POST'):
            response["form"] = forms.PemesananFormForAnggota(request.POST)
            if (response["form"].is_valid()):
                barang = response["form"].cleaned_data["barang"]
                lama_sewa = response["form"].cleaned_data["lama_sewa"]
                request.session["response"]["barang_dipesan"] = barang
                request.session["response"]["lama_barang_dipesan_disewa"] = lama_sewa
                id_pemesanan_anggota = id_pemesanan
                datetime_pemesanan = datetime.datetime.now()
                kuantitas_barang = 1
                harga_sewa = 15000
                ongkos = 10000
                no_ktp_pemesan = request.session['response']['no_ktp_yang_lagi_login']
                status = "DISIAPKAN"
                request.session["response"]["id_pemesanan"] = id_pemesanan
                execute_create_pemesanan(id_pemesanan = id_pemesanan,datetime_pemesanan = datetime_pemesanan,
                kuantitas_barang = kuantitas_barang,harga_sewa=harga_sewa,ongkos=ongkos,no_ktp_pemesan=no_ktp_pemesan,
                status=status)
                id_pemesanan+=1
                return redirect("daftar_pemesanan")
        return render(request,"pemesanan.html",response)   
    
    return redirect("login_page")

    
    # return render(request,'pemesanan.html')

def update_pesanan(request):
    if len(request.session.keys())>0:
        response["form"] = forms.UpdatePemesananForm()
        if request.method == 'POST':
            response["form"] = forms.UpdatePemesananForm(request.POST)
            if response["form"].is_valid():
                barang = response["form"].cleaned_data["barang"]
                lama_sewa = response["form"].cleaned_data["lama_sewa"]
                status = response["form"].cleaned_data["status"]
                execute_update_pemesanan(identifier=request.session["response"]["id_pemesanan"],status=status)
                return redirect("daftar_pemesanan")
        return render(request,"update_pesanan.html",response)   
    return redirect("login_page")

def delete_pesanan(request):
    if len(request.session.keys())>0:
        execute_delete_pemesanan(request.session["response"]["id_pemesanan"])
        request.session["response"]["id_pemesanan"] = None
        return render(request,"daftar_pemesanan.html",response)   
    return redirect("login_page")

def daftar_level(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM TOYS_RENT.LEVEL_KEANGGOTAAN;")
    daftar = dictfetchall(cursor)
    response['daftar'] = daftar
    return render(request,'daftar_level.html', response)
    all_levels = get_as_dict(get_all("level_keanggotaan"))
    if len(request.session.keys())>0:
        if request.session["response"]["login_as_admin"]:
            request.session["response"]["all_level_keanggotaan"] = all_levels
            return render(request,"daftar_level.html",response)
        return redirect("login_page")  
    return redirect("login_page")

def update_level(request,identifier):
    if len(request.session.keys())>0:
        if request.session['response']['login_as_admin']:
            response["form"] = forms.LevelForm()
            if request.method == 'POST':
                response["form"] = forms.LevelForm(request.POST)
                nama_level = response["form"].cleaned_data["nama_level"]
                minimum_poin = response["form"].cleaned_data["minimum_poin"]
                deskripsi = response["form"].cleaned_data["deskripsi"]
                execute_update_level(identifier,nama_level=nama_level,minimum_poin=minimum_poin,deskripsi=deskripsi)
                return redirect("daftar_level")
            return render(request,"update_level.html",response)
        return redirect("login_page")
    return redirect("login_page")

def delete_level(request,identifier):
    if len(request.session.keys())>0:
        if request.session['response']['login_as_admin']:
            execute_delete_level(identifier)
            return redirect("daftar_level")
        return redirect("login_page")
    return redirect("login_page")

def tambah_level(request):
    return render(request,'tambah_level.html')

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
    if len(request.session.keys())>0:
        if request.session['response']['login_as_admin']:
            response["form"] = forms.LevelForm()
            if request.method == 'POST':
                print("post banggg")
                response["form"] = forms.LevelForm(request.POST)
                if response["form"].is_valid():
                    nama_level = response["form"].cleaned_data["nama_level"]
                    minimum_poin = response["form"].cleaned_data["minimum_poin"]
                    deskripsi = response["form"].cleaned_data["deskripsi"]
                    execute_create_level(nama_level=nama_level,minimum_poin=minimum_poin,deskripsi=deskripsi)
                    return redirect("daftar_level")
            return render(request,"tambah_level.html",response) 
        return redirect("login_page")  
    return redirect("login_page")

def execute_create_pemesanan(**kwargs):
    table_name = "pemesanan"
    cursor = connection.cursor()
    target = "toys_rent."+table_name
    query ="insert into {} values({},{},{},{},{},{}.{})".format(target,
    kwargs['id_pemesanan'],kwargs['datetime_pemesanan'],kwargs['kuantitas_barang'],
    kwargs['harga_sewa'],kwargs['ongkos'],kwargs['no_ktp_pemesan'],kwargs['status'])
    cursor.execute(query)
    return cursor


def execute_update_pemesanan(**kwargs):
    """
    Properti Kwargs:
    identifier
    status
    """
    table_name = "pemesanan"
    cursor = connection.cursor()
    target = target = "toys_rent."+table_name
    query = "update {} set {} = {} where {} = {}".format(target,"status",kwargs['status'],"id_pemesanan",kwargs["identifier"])
    cursor.execute(query)
    return cursor
def execute_delete_pemesanan(id_pemesanan):
    table_name = "pemesanan"
    cursor = connection.cursor()
    target = target = "toys_rent."+table_name
    query = "delete from {} where id_pemesanan = {}".format(target,id_pemesanan)
    cursor.execute(query)
    return cursor

def execute_create_level(**kwargs):
    table_name = "level_keanggotaan"
    cursor = connection.cursor()
    target = "toys_rent."+table_name
    cursor.execute("set search_path to toys_rent")
    print(target)
    print(kwargs['nama_level'])
    print(kwargs['minimum_poin'])
    print(kwargs['deskripsi'])
    query = "insert into {} values('{}',{},'{}')".format(target,str(kwargs["nama_level"]),
    int(kwargs["minimum_poin"]),str(kwargs["deskripsi"]))
    cursor.execute(query)
    cursor.execute("set search_path to public")
    return cursor

def execute_update_level(identifier,**kwargs):
    table_name = "level_keanggotaan"
    cursor = connection.cursor()
    target = "toys_rent."+table_name
    query = "update {} set nama_level = {}, minimum_poin = {}, deskripsi = {} where nama_level = {}".format(
        target,kwargs['nama_level'],kwargs['minimum_poin'],kwargs['deskrips'],identifier)
    cursor.execute(query)
    return cursor

def execute_delete_level(identifier):
    table_name = "level_keanggotaan"
    cursor = connection.cursor()
    target = "toys_rent."+table_name
    query = "delete from {} where nama_level = {}".format(target,identifier)
    cursor.execute(query)